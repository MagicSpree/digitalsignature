import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.security.KeyPair;

class DigitalSignatureTest {

    final static String SIMPLE_TEXT = "Simple Text";
    DigitalSignature digitalSignature = new DigitalSignature();

    @Test
    void verifyDigitalSignature() throws Exception {

        KeyPair keyPair = digitalSignature.generationKeyPair();
        digitalSignature.initSignatureObj(keyPair.getPrivate());

        byte[] sign = digitalSignature.createDigitalSignature(SIMPLE_TEXT.getBytes(StandardCharsets.UTF_8));

        boolean verify = digitalSignature.verifyDigitalSignature(keyPair.getPublic(), SIMPLE_TEXT.getBytes(StandardCharsets.UTF_8), sign);
        Assertions.assertTrue(verify);
    }

    @Test
    void verifyDigitalSignature_WhenDifferentKeys() throws Exception {

        KeyPair keyPair = digitalSignature.generationKeyPair();
        digitalSignature.initSignatureObj(keyPair.getPrivate());

        byte[] sign = digitalSignature.createDigitalSignature(SIMPLE_TEXT.getBytes(StandardCharsets.UTF_8));

        keyPair = digitalSignature.generationKeyPair();

        boolean verify = digitalSignature.verifyDigitalSignature(keyPair.getPublic(), SIMPLE_TEXT.getBytes(StandardCharsets.UTF_8), sign);
        Assertions.assertFalse(verify);
    }

    @Test
    void verifyDigitalSignature_WhenAnotherText() throws Exception {

        KeyPair keyPair = digitalSignature.generationKeyPair();
        digitalSignature.initSignatureObj(keyPair.getPrivate());

        byte[] sign = digitalSignature.createDigitalSignature(SIMPLE_TEXT.getBytes(StandardCharsets.UTF_8));

        keyPair = digitalSignature.generationKeyPair();

        boolean verify = digitalSignature.verifyDigitalSignature(keyPair.getPublic(), "SIMPLE_TEXT".getBytes(StandardCharsets.UTF_8), sign);
        Assertions.assertFalse(verify);
    }

}