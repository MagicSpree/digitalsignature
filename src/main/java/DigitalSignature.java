import java.security.*;

public class DigitalSignature {

    private Signature dsa;

    public KeyPair generationKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA");
        keyGen.initialize(1024, new SecureRandom());
        return keyGen.generateKeyPair();
    }

    public void initSignatureObj(PrivateKey privateKey)
            throws NoSuchAlgorithmException, InvalidKeyException {

        dsa = Signature.getInstance("SHA/DSA");
        dsa.initSign(privateKey);
    }

    public byte[] createDigitalSignature(byte[] bytesPlainText) throws SignatureException {
        dsa.update(bytesPlainText);
        return dsa.sign();
    }

    public boolean verifyDigitalSignature(PublicKey publicKey, byte[] bytesPlainText, byte[] cipherText)
            throws InvalidKeyException, SignatureException {

        dsa.initVerify(publicKey);
        dsa.update(bytesPlainText);

        return dsa.verify(cipherText);
    }
}
