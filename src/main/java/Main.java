import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Scanner;

public class Main {

    static DigitalSignature digitalSignature = new DigitalSignature();

    public static void main(String[] args) throws Exception {

        System.out.println("Введите сообщение: ");
        Scanner keyboard = new Scanner(System.in);
        String inputText = keyboard.nextLine();

        KeyPair keyPair = digitalSignature.generationKeyPair();
        PrivateKey privateKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();

        byte[] byteText = inputText.getBytes(StandardCharsets.UTF_8);
        byte[] cipherText = setDigitalSignature(byteText, privateKey);

        boolean verify = digitalSignature.verifyDigitalSignature(publicKey, byteText, cipherText);
        String result = verify ? "Сообщение подлинно" : "Сообщение не является подлинным";
        System.out.println(result);
    }

    public static byte[] setDigitalSignature(byte[] byteText, PrivateKey privateKey) throws Exception {
        digitalSignature.initSignatureObj(privateKey);
        return digitalSignature.createDigitalSignature(byteText);
    }
}
